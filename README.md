# A WordPress development environment with Docker Compose on WSL

Quickly set up a development environment for [Chthonic](https://chthonic.nz) WordPress projects, using Docker containers on the Windows Subsystem for Linux (WSL).

This is a fork of [chriszarate/docker-compose-wordpress](https://github.com/chriszarate/docker-compose-wordpress).

## Set up

1. Clone this repo with the SSH URL to the `..Dropbox/projects` folder. Rename this folder to identify the project.

2. Edit docker-compose.yml to reflect project content that should be persisted via `services:volumes`, e.g:

    - `theme`
    
        Clone the project theme to this location.
    
    - `plugins`
    
        Clone a project plugin to this location and/or include 3rd-party plugins required for the project.
    
    - `uploads`
    
        Access and manage project media at this location.

    - `wp-data`

        A .sql file dropped here will be used as the WordPress installation's database.

3. Edit .env to set the project vars, e.g:

    - `CHTHONIC_DEV_DOMAIN`
        
        Sets the local development domain (using 'test' as the tld), e.g: project.test.

    - `CHTHONIC_WP_TITLE`
        
        Sets the `Site Title` of the WordPress installation.
    
    - `CHTHONIC_THEME_FOLDER`
        
        Sets the folder name of the theme. 

4. Add the value set for `CHTHONIC_DEV_DOMAIN` to `C:/Windows/System32/drivers/etc/hosts`, as follows:

    ```
    127.0.0.1 localhost project.test
    ```

## Start environment

```sh
docker-compose up -d
```

Important: if volumes don't mount the first thing to check is the path used 
to reach the project folder. See the Volumes section in "Running Docker on WSL".

The first time this is run, it will take a few minutes to pull in the required
images. On subsequent runs, it should take less than 30 seconds before it's 
possible to connect to WordPress in a browser. (Most of this time is waiting 
for MariaDB to be ready to accept connections.)

The `-d` flag backgrounds the process and log output. To view logs for a
specific container, use `docker-compose logs [container]`, e.g.:

```sh
docker-compose logs wordpress
```

## Accessing the WordPress admin

Log in to `/wp-admin/` with `wordpress:wordpress`. Use existing credentials if importing a database via the `wp-data` volume.

## Accessing phpMyAdmin

Log in to `project.test:8181` with `root` as the username. Do not enter a password.

## Update environment

To pull in the latest images, make sure the clone/fork of this repo is up to 
date, then run the following commands. Note that this will **destroy** the 
current environment, including the database (if not persisted via a volume at `mysql:volumes`), and reset it to its initial state.

```sh
docker-compose down
docker-compose pull
docker-compose up -d
```

## Running Docker on WSL

### Docker for Windows

The Docker daemon does not run directly on WSL. Installing Docker for Windows will expose a remote Docker daemon for the WSL Docker CLI to utilise.

To connect the Docker on Windows Daemon with the WSL Docker CLI:

- In the Docker for Windows settings: General > tick "Expose daemon on tcp://localhost:2375 without TLS.

- Add the following to `~/.bashrc`: `export DOCKER_HOST=tcp://0.0.0.0:2375`.

- Exit the shell, then open a new shell and confirm the connection is working with `docker info`.

Source: [Setting up Docker for Windows and WSL to work flawlessly](https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly).

### Volumes

WSL mounts the C: drive at /mnt/c and Docker's base VM mounts it at /c. This prevents mounting working as expected.

A work-around is available:

- `$ sudo mkdir /c`

- `$ sudo mount --bind /mnt/c /c`

- `$ cd /c/path/to/project`

- `$ docker-compose up -d`

Note: the bound mount persists for the life of the shell.

Source: [Build fails on Windows Subsystem for Linux (WSL)](https://github.com/10up/wp-local-docker/issues/81).

[image]: https://hub.docker.com/r/chriszarate/wordpress/
[docker-compose]: https://docs.docker.com/compose/
[mariadb-docs]: https://github.com/docker-library/docs/tree/master/mariadb#initializing-a-fresh-instance

### Local DNS

An entry for the development domain must be added to the Windows host file.

```sh
127.0.0.1 localhost project.test
```